# setwd("C:/Users/Catarina/nn_shiny")
library(nnet)

load("temp/mapping.RData")
load("temp/param.RData")

#### FUNCTIONS ####
normalize <- function(dataset,s=NULL,m=NULL){
  RET <- NULL
  RET$new.dataset <- NULL
  RET$sds <- NULL
  RET$means <- NULL
  i <- 1
  if(is.null(s) & is.null(m)){
    for(index in 1:ncol(dataset)){
      column <- dataset[,index]
      if(is.factor(column)){
        if(is.null(RET$new.dataset)){
          RET$new.dataset <- data.frame(column)
        } else {
          RET$new.dataset <- data.frame(RET$new.dataset,as.character(column))
        }
        colnames(RET$new.dataset)[i] <- colnames(dataset)[i]
        RET$sds <- c(RET$sds,NA)
        RET$means <- c(RET$means,NA)
        i <- i+1  
      } else {
        stdDev <- sd(column,na.rm=TRUE)
        if(stdDev==0){
          c <- column
        } else {
          c <- (column-mean(column,na.rm=T))/stdDev
        }
        if(is.null(RET$new.dataset)){
          RET$new.dataset <- data.frame(c)
        } else {
          RET$new.dataset <- data.frame(RET$new.dataset,c)
        }
        colnames(RET$new.dataset)[i] <- colnames(dataset)[i]
        RET$sds <- c(RET$sds,stdDev)
        RET$means <- c(RET$means,mean(column))
        i <- i+1
      }
    }
  } else {
    for(index in 1:ncol(dataset)){
      column <- dataset[,index]
      if(is.factor(column)){
        if(is.null(RET$new.dataset)){
          RET$new.dataset <- data.frame(column)
        } else {
          RET$new.dataset <- data.frame(RET$new.dataset,column)
        }
        colnames(RET$new.dataset)[i] <- colnames(dataset)[i]
        RET$sds <- c(RET$sds,NA)
        RET$means <- c(RET$means,NA)
        i <- i+1
      } else {
        stdDev <- s[i]
        if(stdDev==0){
          c <- column
        } else {
          c <- (column-m[i])/stdDev
        }
        if(is.null(RET$new.dataset)){
          RET$new.dataset <- data.frame(c)
        } else {
          RET$new.dataset <- data.frame(RET$new.dataset,c)
        }
        colnames(RET$new.dataset)[i] <- colnames(dataset)[i]
        RET$sds <- c(RET$sds,stdDev)
        RET$means <- c(RET$means,mean(column))
        i <- i+1
      }
    }
  }
  RET
}

prep.normalize <- function(dataset, norm, folds, fold_index){
  RET <- NULL
  t <- normalize(dataset[which(folds!=fold_index, arr.ind=TRUE),])
  RET$trainset <- t$new.dataset
  RET$means <- t$means
  RET$sds <- t$sds
  RET$testset <- normalize(dataset[which(folds==fold_index, arr.ind=TRUE),],s=RET$sds,m=RET$means)$new.dataset
  RET
}

landmarkers <- NULL

# ds <- read.table("temp/data.data",sep=" ",dec=".",header=T,na.strings="NA")
load("temp/data.RData")
ds<-df
testsize <- round(nrow(ds)*0.2,digits = 0)
trainsize <- nrow(ds)-testsize
folds <- sample(c(rep(1, length.out=testsize),rep(2, length.out=nrow(ds)-(nrow(ds)*0.2))))
trainset <- ds[folds==2,]
testset <- ds[folds==1,]

confs <- mapping
for(ci in 1:nrow(confs)){
  conf <- confs[ci,]
  wts <- as.numeric(strsplit(as.character(conf$weights), split = ",")[[1]])
  
  model0 <- nnet(formula=target ~ ., data=trainset, size=as.numeric(param$nearest[,"neurons"]), decay=as.numeric(param$nearest[,"decay"]), abstol=as.numeric(param$nearest[,"abstol"]), linout=TRUE, MaxNWts=100000, maxit=0, Wts=wts)
  predictions0 <- predict(object=model0, newdata=testset)
  mse0 <- mean((predictions0-testset$target)^2, na.rm=TRUE)
  
  time.i <- proc.time()
  model1 <- nnet(formula=target ~ ., data=trainset, size=as.numeric(param$nearest[,"neurons"]), decay=as.numeric(param$nearest[,"decay"]), abstol=as.numeric(param$nearest[,"abstol"]), linout=TRUE, MaxNWts=100000, maxit=1, Wts=wts)
  time.f <- proc.time()
  predictions1 <- predict(object=model1, newdata=testset)
  mse1 <- mean((predictions1-testset$target)^2, na.rm=TRUE)
  time1 <- as.numeric((time.f-time.i)[1])
  learn1 <- mse0-mse1
  dif.wts1 <- model1$wts - wts
  w.mean1 <- mean(dif.wts1)
  w.sd1 <- sd(dif.wts1)
  w.cv1 <- w.sd1/w.mean1
  
  time.i <- proc.time()
  model10 <- nnet(formula=target ~ ., data=trainset, size=as.numeric(param$nearest[,"neurons"]), decay=as.numeric(param$nearest[,"decay"]), abstol=as.numeric(param$nearest[,"abstol"]), linout=TRUE, MaxNWts=100000, maxit=10, Wts=wts)
  time.f <- proc.time()
  predictions10 <- predict(object=model10, newdata=testset)
  mse10 <- mean((predictions10-testset$target)^2, na.rm=TRUE)
  time10 <- as.numeric((time.f-time.i)[1])
  learn10 <- mse0-mse10
  dif.wts10 <- model10$wts - wts
  w.mean10 <- mean(dif.wts10)
  w.sd10 <- sd(dif.wts10)
  w.cv10 <- w.sd10/w.mean10
  
  time.i <- proc.time()
  model100 <- nnet(formula=target ~ ., data=trainset, size=as.numeric(param$nearest[,"neurons"]), decay=as.numeric(param$nearest[,"decay"]), abstol=as.numeric(param$nearest[,"abstol"]), linout=TRUE, MaxNWts=100000, maxit=100, Wts=wts)
  time.f <- proc.time()
  predictions100 <- predict(object=model100, newdata=testset)
  mse100 <- mean((predictions100-testset$target)^2, na.rm=TRUE)
  time100 <- as.numeric((time.f-time.i)[1])
  learn100 <- mse0-mse100
  dif.wts100 <- model100$wts - wts
  w.mean100 <- mean(dif.wts100)
  w.sd100 <- sd(dif.wts100)
  w.cv100 <- w.sd10/w.mean100
  
  landmarkers <- rbind(landmarkers,
                       cbind(ds=as.character(conf$ds), dt=as.character(conf$dt), 
                             mse0, mse1, time1, learn1, w.sd1, w.mean1, w.cv1,
                             mse10, time10, learn10, w.sd10, w.mean10, w.cv10,
                             mse100, time100, learn100, w.sd100, w.mean100, w.cv100)
  )
}
landmarkers <- data.frame(landmarkers)
landmarkers$ds <- as.character(landmarkers$ds)
landmarkers$dt <- as.character(landmarkers$dt)

for(i in 3:ncol(landmarkers)){
  landmarkers[,i] <- as.numeric(as.character(landmarkers[,i]) )
}

save(landmarkers, file="temp/landmarkers.RData")
