# setwd("C:/Users/Catarina/nn_shiny/")
library(nnet)

load(file="temp/dataset.names.RData")
source("R/aux_code/functions.R")


dataset <- read.table(paste0("regression/",dataset.names[1],"/",dataset.names[1],".data"), sep=" ", header = F)
colnames(dataset) <- as.character(read.table(paste0("regression/",dataset.names[1],"/",dataset.names[1],".names"), sep = ":", header = F)[,1])

nreps <- 1


# por isto mais rapido: calcular so os landmarkers que interessam a partir das formulas
# neurons -> "target ~ lm_0_it_abstol"
# decay -> "target ~ lm_0_it_abstol"
# abstol -> "target ~ lm_0_it_abstol + lm_1h_best_abstol + lm_1_it_abstol + lm_10_1_0.001_2_NA_w.mean + lm_10_0.1_0.001_2_NA_w.mean + lm_10_0.001_0.001_2_NA_w.mean"
# lm_0_it_abstol
# lm_1h_best_abstol
# lm_1_it_abstol
# lm_10_1_0.001_2_NA_w.mean
# lm_10_0.1_0.001_2_NA_w.mean
# lm_10_0.001_0.001_2_NA_w.mean

data <- train.test(dataset, 0.8)
nattr <- ncol(dataset)-1
final <- NULL
for(n in c(1,3,5,10,20)){
  for(r in c(1:nreps)){
    inweights <- bishop.weights(size = weights.size(nattr = nattr, nhidden = n), nattr = nattr)
    for(d in c(1, 0.1, 0.01, 0.001, 0.0001)){
      for(a in c(0.001, 0.0001, 0.00001)){
        if(n==1){
          res <- execute.nnet(train = data$train,
                              test = data$test,
                              nhidden = n,
                              decay = d,
                              abstol = a,
                              weights = inweights)
          aux <- matrix(unlist(res)[1:5],nrow = 1)
          colnames(aux) <- names(res)[1:5]
          final <- rbind(final,data.frame(cbind(ds=dataset.names[1], rep=r, n=n, d=d, a=a, ss=NA, maxit=NA, aux)))
        } else {
          m <- 0
          res <- execute.nnet(train = data$train,
                              test = data$test,
                              nhidden = n,
                              decay = d,
                              abstol = a,
                              weights = inweights,
                              maxIt = m)
          aux <- matrix(unlist(res)[1:5],nrow = 1)
          colnames(aux) <- names(res)[1:5]
          final <- rbind(final,data.frame(cbind(ds=dataset.names[1], rep=r, n=n, d=d, a=a, ss=NA, maxit=m, aux)))
          inweights2 <- res$wts
          for(m in 1:2){#1:10){
            res <- execute.nnet(train = data$train,
                                test = data$test,
                                nhidden = n,
                                decay = d,
                                abstol = a,
                                weights = inweights2,
                                maxIt = 1)
            aux <- matrix(unlist(res)[1:5],nrow = 1)
            colnames(aux) <- names(res)[1:5]
            final <- rbind(final,data.frame(cbind(ds=dataset.names[1], rep=r, n=n, d=d, a=a, ss=NA, maxit=m, aux)))
          }
          # for(ss in c(10,25,50,100,as.integer(0.1*nrow(dataset)))){
          #   if(ss < nrow(dataset) && ss > 5){
          #     sampledata <- dataset[sample(1:nrow(dataset), size=ss, replace = F),]
          #     data2 <- train.test(sampledata,pc=0.8)
          #     res <- execute.nnet(train = data2$train,
          #                         test = data2$test,
          #                         nhidden = n,
          #                         decay = d,
          #                         abstol = a,
          #                         weights = inweights)
          #     aux <- matrix(unlist(res)[1:5],nrow = 1)
          #     colnames(aux) <- names(res)[1:5]
          #     final <- rbind(final,data.frame(cbind(ds=dataset.names[1], rep=r, n=n, d=d, a=a, ss=ss, maxit=NA, aux)))
          #   } else {
          #     final <- rbind(final,data.frame(cbind(ds=dataset.names, rep=r, n=n, d=d, a=a, ss=ss, maxit=NA, aux)))
          #   }
          # }
        }
      }
    }
  }
}

# # 1h
# data <- train.test(dataset, 0.8)
# nattr <- ncol(dataset)-1
# final <- NULL
# for(n in c(1,3,5,10,20)){
#   for(r in c(1:nreps)){
#     inweights <- bishop.weights(size = weights.size(nattr = nattr, nhidden = n), nattr = nattr)
#     for(d in c(1, 0.1, 0.01, 0.001, 0.0001)){
#       for(a in c(0.001, 0.0001, 0.00001)){
#         if(n==1){
#           res <- execute.nnet(train = data$train,
#                               test = data$test,
#                               nhidden = n,
#                               decay = d,
#                               abstol = a,
#                               weights = inweights)
#           aux <- matrix(unlist(res)[1:5],nrow = 1)
#           colnames(aux) <- names(res)[1:5]
#           final <- rbind(final,data.frame(cbind(ds=dataset.names[1], rep=r, n=n, d=d, a=a, ss=NA, maxit=NA, aux)))
#         } else {
#           m <- 0
#           res <- execute.nnet(train = data$train,
#                               test = data$test,
#                               nhidden = n,
#                               decay = d,
#                               abstol = a,
#                               weights = inweights,
#                               maxIt = m)
#           aux <- matrix(unlist(res)[1:5],nrow = 1)
#           colnames(aux) <- names(res)[1:5]
#           final <- rbind(final,data.frame(cbind(ds=dataset.names[1], rep=r, n=n, d=d, a=a, ss=NA, maxit=m, aux)))
#           inweights2 <- res$wts
#           for(m in 1:10){
#             res <- execute.nnet(train = data$train,
#                                 test = data$test,
#                                 nhidden = n,
#                                 decay = d,
#                                 abstol = a,
#                                 weights = inweights2,
#                                 maxIt = 1)
#             aux <- matrix(unlist(res)[1:5],nrow = 1)
#             colnames(aux) <- names(res)[1:5]
#             final <- rbind(final,data.frame(cbind(ds=dataset.names[1], rep=r, n=n, d=d, a=a, ss=NA, maxit=m, aux)))
#           }
#           for(ss in c(10,25,50,100,as.integer(0.1*nrow(dataset)))){
#             if(ss < nrow(dataset) && ss > 5){
#               sampledata <- dataset[sample(1:nrow(dataset), size=ss, replace = F),]
#               data2 <- train.test(sampledata,pc=0.8)
#               res <- execute.nnet(train = data2$train,
#                                   test = data2$test,
#                                   nhidden = n,
#                                   decay = d,
#                                   abstol = a,
#                                   weights = inweights)
#               aux <- matrix(unlist(res)[1:5],nrow = 1)
#               colnames(aux) <- names(res)[1:5]
#               final <- rbind(final,data.frame(cbind(ds=dataset.names[1], rep=r, n=n, d=d, a=a, ss=ss, maxit=NA, aux)))
#             } else {
#               final <- rbind(final,data.frame(cbind(ds=dataset.names, rep=r, n=n, d=d, a=a, ss=ss, maxit=NA, aux)))
#             }
#           }
#         }
#       }
#     }
#   }
# }


save(final,file = "temp/final.RData")
